var express = require('express');
var bodyParser = require('body-parser');

var paul = {

    replies: {
        'To you': 'To me',
        'To me': 'To you'
    },

    chuckle: function(req) {

        if ((req.body.paul) && (req.body.barry)) {
            return this.ohDearOhDear();
        }
        if ((!req.body.paul) && (!req.body.barry)) {
            return this.ohDearOhDear();
        }

        var brother = req.body.paul ? 'paul' : 'barry';
        var reply = this.replies[req.body[brother]];
        var replyBrother = req.body.paul ? 'barry' : 'paul';

        if (!reply) {
            return this.ohDearOhDear();
        }

        var response = {};
        response[replyBrother] = reply;

        return response;
    },

    ohDearOhDear: function() {
        return {
            'paul': 'Oh dear oh dear',
            'barry': 'Oh dear oh dear'
        };
    },

    start: function(port) {

        var _this = this;

        var app = express();

        app.use(bodyParser.json());

        app.post('/', function(req, res){
            res.send(JSON.stringify(_this.chuckle(req)));
        });

        app.listen(port || 3101);
    }
};

module.exports = {
    start: function(port) {
        paul.start(port);
    }
};
