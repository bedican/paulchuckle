# PaulChuckle

PaulChuckle is the server component of the [ChuckleBrothers](https://www.npmjs.com/package/chucklebrothers)

## Installation

``` bash
  $ npm install paulchuckle
```

#### License: MIT
#### Author: [Ash Brown](https://bitbucket.org/bedican)
